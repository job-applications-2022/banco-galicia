#!/bin/sh
set -e

# Get terraform ServiceAccount
doppler run -- printenv "IAM_SA_CLOUD_RUN_64" | base64 -d > $(pwd)/credential.json

# Authenticate with terraform ServiceAccount
gcloud auth activate-service-account $(doppler run -- printenv "IAM_SA_CLOUD_RUN_EMAIL") --key-file=$(pwd)/credential.json --project=ninja-sre

gcloud run deploy banco-galicia-interview --image=us-east1-docker.pkg.dev/ninja-sre/job-interviews-2022/banco-galicia:$CI_COMMIT_SHORT_SHA \
    --platform managed --max-instances=5 --min-instances=1 --allow-unauthenticated --region=us-east1 --port 80 --revision-suffix $CI_COMMIT_SHORT_SHA

gcloud run services update-traffic banco-galicia-interview --to-revisions banco-galicia-interview-$CI_COMMIT_SHORT_SHA=100 --region=us-east1
